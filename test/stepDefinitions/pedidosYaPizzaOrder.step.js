import { defineSupportCode } from 'cucumber';
import PedidosYaHome from "../pageobjects/pedidos-ya-home.page";
import ModalUbicacionDeEntrega from "../pageobjects/modal-ubicacion-de-entrega.page";
import SearchResult from "../pageobjects/search-result.page";
import Restaurant from "../pageobjects/restaurant.page";
import World from "../world";

defineSupportCode(function({ Given }) {

    Given(/^I am on the pedidosYaHome page$/, function() {
        PedidosYaHome.open('https://www.pedidosya.com.uy/');
        browser.getTitle().should.equal('Delivery de Comida Online - Elegí, Pedí y Ya | PedidosYa');
    });
});


defineSupportCode(function({ When }) {

    When(/^I set any street and search for "Pizza"$/, function() {
        PedidosYaHome.waitForPedidosYaHomePageToLoad();
        PedidosYaHome.setFood('Pizza');
        PedidosYaHome.setAddress('Rio Branco 1358');
    });

    When(/^I click the search button without moving the location on the modal$/, function() {
        ModalUbicacionDeEntrega.waitForModalUbicacionDeEntregaPageToLoad();
        ModalUbicacionDeEntrega.confirmarUbicacion();
        SearchResult.waitForSearchResultPageToLoad();


    });

    When(/^I report total no. of search results with no. of results in the current page$/, function() {
        SearchResult.logResultsData();
    });


    When(/^I select a random Filter$/, function() {
        SearchResult.selectRandomFilter();
        SearchResult.waitForSearchResultPageToLoad();
    });

    When(/^I verify the number of results is correct$/, function() {
        SearchResult.calculateRestaurantAmountAndMoveToLastPage();
        SearchResult.getDeclaredNumberOfRestaurantByPage().should.equal(World.caculatedAmount)
    });

    When(/^I sort, the restaurants, "alphabetically"$/, function() {
        SearchResult.clickOrderingList();
        SearchResult.clickAlphaOrdering();
        SearchResult.waitForSearchResultPageToLoad();
        SearchResult.getAllOpenedRestaurants();
    });

    When(/^I verify the sort is correct, for the opened restaurants$/, function() {
        SearchResult.checkIfAlphaOrdered().should.be.true;
    });

});

defineSupportCode(function({ Then }) {

    Then(/^I log all the requested reports and check the information is correct$/, function() {
        SearchResult.logResultsAfterFilter();
        SearchResult.reportTheStartsForEachOfTheResultsInTheFirstResultPage();
        SearchResult.goToFirstRestaurant();
        Restaurant.waitForRestaurantPageToLoad();
        Restaurant.logCriticalInformation();
        Restaurant.logOpinions();
    });

});


