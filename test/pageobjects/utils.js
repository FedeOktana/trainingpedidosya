class Utils {

    getRandomItemFromArray(array) {
        return array[Math.floor(Math.random() * array.length)];
    }

    arraysAreEqual(a, b) {
        for (let i = 0; i < a.length; ++i) {
            if (a[i] !== b[i]) return false;
        }
        return true;
    }

    copyArrayAndSort(array) {
        let copy = array;
        return copy.sort();
    }


}

export default new Utils()