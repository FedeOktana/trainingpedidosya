class ModalUbicacionDeEntrega {

    /**
     * define elements
     */

    get locationIcon() {
        return browser.element('.location-icon-map');
    }

    get confirmarUbicacionButton() {
        return browser.element('#confirm')
    }

    /**
     * define or overwrite page methods
     */

    waitForModalUbicacionDeEntregaPageToLoad() {
        if (!this.locationIcon.isVisible()) {
            this.locationIcon.waitForVisible(3000);
        }
    }

    confirmarUbicacion() {
        this.confirmarUbicacionButton.click();
    }
}

export default new ModalUbicacionDeEntrega()
