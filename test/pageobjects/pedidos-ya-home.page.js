import Page from './page'

class PedidosYaHome extends Page {

    /**
     * define elements
     */

    get recibiTuComidaImg() {
        return browser.element('ul.container_steps > li:nth-child(3) > img');
    }

    get adressInput() {
        return browser.element('#address')
    }

    get searchButton() {
        return browser.element('#search')
    }

    get foodInput() {
        return browser.element('#optional')
    }

    /**
     * define or overwrite page methods
     */
    open(url) {
        super.open(url);      //this will append `contact-us` to the baseUrl to form complete URL
        browser.pause(2000);
    }

    waitForPedidosYaHomePageToLoad() {
        if (!this.recibiTuComidaImg.isVisible()) {
            this.recibiTuComidaImg.waitForVisible(3000);
        }
    }

    setFood(food) {
        this.foodInput.setValue(food)
    }

    setAddress(address) {
        this.adressInput.setValue(address);
        this.searchButton.click();
    }
}

export default new PedidosYaHome()
