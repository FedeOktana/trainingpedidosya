class Restaurant {

    /**
     * define elements
     */

    get getRestaurantName() {
        return browser.element('#breadcrumbContainer > div > div > h2');
    }

    get uruguayIcon() {
        return browser.element('a.uy[title="PedidosYa Uruguay"]');
    }

    get opinionsButton() {
        return browser.element('button[data-auto="shopdetails_tabs_reviews"]')
    }

    get thirdOpinionImg() {
        return browser.element('#reviewList > li:nth-child(3) > figure > img')
    }

    get getStrasLis(){
        return browser.elements('.review_list > li')
    }

    get criticalInformationSpans() {
        return browser.elements('#profileDetails > span[title]')
    }

    get getAllReviews() {
        return browser.elements('#reviewList > li[itemprop="review"]')
    }

    /**
     * define or overwrite page methods
     */

    waitForRestaurantPageToLoad() {
        if (!this.uruguayIcon.isVisible()) {
            this.uruguayIcon.waitForVisible(5000);
        }
    }

    logCriticalInformation(){
        console.log('\nCRITICAL INFORMATION FOR ' +  this.getRestaurantName.getText());
        for(let info of this.criticalInformationSpans.value){
            console.log(info.getAttribute('title'))
        }
        console.log('\nRATINGS');
        for(let starInfo of this.getStrasLis.value){
            console.log(starInfo.getText())
        }

    }

    waitFor3OpinionsToLoad() {
        if (!this.thirdOpinionImg.isVisible()) {
            this.thirdOpinionImg.waitForVisible(3000)
        }
    }



    logOpinions() {
        this.opinionsButton.click();
        this.waitFor3OpinionsToLoad();
        console.log("\n3 OPINIONS");
        for (let [index, value] of this.getAllReviews.value.entries()) {
            let author = value.elementIdElement(value.value.ELEMENT , 'section > span > div').getText();
            let message = value.elementIdElement(value.value.ELEMENT , 'section > p').getText();
            console.log(author + ' said: ' + message);
            if(index===2) break;
        }

    }

}

export default new Restaurant()
