import Utils from './utils'
import World from './../world.js';

class SearchResult {

    /**
     * define elements
     */

    get uruguayIcon() {
        return browser.element('a.uy[title="PedidosYa Uruguay"]');
    }

    get restaurantsData() {
        return browser.elements('.restaurantData')
    }

    get localesAmonut() {
        return browser.element('h1[data-total] > i')
    }

    get orderList() {
        return browser.element('a[data-auto="shoplist_ordercontainer"]')
    }

    get listPossibleFilters() {
        return browser.elements('.js-filter-channel.old_channel')
    }

    get alphabeticalOrderLink() {
        return browser.element('li[value="az"] > a')
    }

    get paginationUl() {
        return browser.element('.pagination')
    }

    get firstRestaurantLink() {
        return browser.element('.restaurant-wrapper[title]:first-child')
    }

    get allOpenedRestaurants() {
        return browser.elements('#resultList > li[data-status^="OPEN"]')
    }

    get allRestaurants() {
        return browser.elements('#resultList > li[data-status]')
    }




    /**
     * define or overwrite page methods
     */

    waitForSearchResultPageToLoad() {
        if (!this.uruguayIcon.isVisible()) {
            this.uruguayIcon.waitForVisible(20000);
        }
    }

    getPageResults() {
        return this.localesAmonut.getText();
    }

    getQueryResults() {
        return this.restaurantsData.value.length;
    }

    getDeclaredNumberOfRestaurantByPage() {
        return Number(this.localesAmonut.getText())
    }


    logResultsData() {
        console.log("HERE START REPORTS");
        console.log("4. Report total no. of Search results with no. of results in the current page -> total: " + this.localesAmonut.getText() + " - on current page: " + this.restaurantsData.value.length);
    }

    logResultsAfterFilter() {
        console.log("REPORT AFTER RANDOM FILTER");
        console.log("9. Report total no. of Search results with no. of results in the current page -> total: " + this.localesAmonut.getText() + " - on current page: " + this.restaurantsData.value.length);
    }


    selectRandomFilter() {
        let filters = this.listPossibleFilters.value;
        let random_item = Utils.getRandomItemFromArray(filters);
        random_item.click();
    }

    clickOrderingList() {
        this.orderList.click()
    }

    clickAlphaOrdering() {
        if (!this.alphabeticalOrderLink.isVisible()) this.alphabeticalOrderLink.waitForVisible(3000);
        this.alphabeticalOrderLink.click();
    }

    getAllOpenedRestaurants() {
        let openRestaurantNames = [];
        for (let restaurant of this.allOpenedRestaurants.value) {
            openRestaurantNames.push(restaurant.getAttribute('title'))
        }
        World.openRestaurantNames = openRestaurantNames;
    }


    calculateRestaurantAmountAndMoveToLastPage() {
        if (this.paginationUl.isVisible()) {
            let lastPaginationListNumber = browser.element('[data-auto="pagination_item"]:last');
            World.currentUrl = browser.getUrl();
            World.caculatedAmount = 50 * ((Number(lastPaginationListNumber.getText())) - 1);
            lastPaginationListNumber.click();
            this.waitForSearchResultPageToLoad();
            World.caculatedAmount += this.getQueryResults();
            browser.url(World.currentUrl);
            this.waitForSearchResultPageToLoad();

        } else {
            World.caculatedAmount = this.getQueryResults();
        }

    }


    checkIfAlphaOrdered() {
        let alphaSortedArray = Utils.copyArrayAndSort(World.openRestaurantNames);
        return Utils.arraysAreEqual(World.openRestaurantNames, alphaSortedArray)
    }

    reportTheStartsForEachOfTheResultsInTheFirstResultPage() {
        for (let restaurant of this.allRestaurants.value) {
            let restaurantName = restaurant.getAttribute('title');
            if(!browser.elementIdElement(restaurant.value.ELEMENT , 'i.rating-points').isVisible()) {
                continue
            }
            let restaurantStars = browser.elementIdElement(restaurant.value.ELEMENT , 'i.rating-points').getText();
            console.log('RESTAURANT: ' + restaurantName + ' has ' + restaurantStars)
        }
    }

    goToFirstRestaurant() {
        browser.url(this.firstRestaurantLink.getAttribute('data-url'))
    }


}

export default new SearchResult()
