Feature: Order pizza from pedidos ya

  As a user on the pedidosYa home page
  I want search for Pizza and see the resulting restaurants
  Because I want to order Pizza

  Background:
    Given I am on the pedidosYaHome page


  Scenario: Performing a pizza search operation
    When I set any street and search for "Pizza"
    And  I click the search button without moving the location on the modal
    And  I report total no. of search results with no. of results in the current page
    And  I select a random Filter
    And  I verify the number of results is correct
    And  I sort, the restaurants, "alphabetically"
    And  I verify the sort is correct, for the opened restaurants
    Then I log all the requested reports and check the information is correct
